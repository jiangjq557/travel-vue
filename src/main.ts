import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
//配置请求数据
import {AxiosInstance } from "axios";
import Axios from "axios";
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

//全局配置Axios
declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $axios: AxiosInstance;
    }
}
import * as Icons from '@ant-design/icons-vue';
const app = createApp(App)
const icons: any = Icons
console.log(icons)
for (const i in icons) {
    app.component(i, icons[i])
}
app.use(store)
app.use(router)
app.mount('#app')
app.use(Antd);
app.use(ElementPlus);
