import service from './index';
const token = localStorage.getItem('token')
export default {
    //获取轮播图
    getBanners() {
        return service.get(`http://157.122.54.189:9095/scenics/banners`)
    },
    //获取验证码
    getCaptchas(phone: string) {
        return service.post(`/user/sendCode`, { phone })
    },
    //注册
    register({ username, phone, code, password }: { username: string; phone: string; code: string; password: string }) {
        return service.post(`/user/insert`, { username, phone, code, password })
    },
    //登录
    login({ phone, password }: { phone: string; password: string }) {
        return service.post(`/user/login`, { phone, password })
    },
    // 查找城市
    getCities(county: string) {
        return service.get(`/route/getPosition?county=${county}`)
    },
    // 查找城市坐标
    getCityPosition(city:string,county: string) {
        return service.get(`/route/getPosition/${city}/${county}`)
    },
    // 城市菜单列表
    getCityList() {
        return service.get(`/route/getArea`)
    },
    // 规划路线
    drawRoute(areaList:any) {
        return service.post(`/route/drawRoute`,{areaList})
    },
    //添加文章
    insertPost(title: string, phone: string, content: string) {
        return service.post(`/posts/insertPost`,{title, phone, content})
    },
    //  文章列表
    getPosts(city: number | string) {
        return service.get(`/posts?city=${city}`)
    },
    //  获取所有文章列表
    getPostAll() {
        return service.get(`/posts/selectAll`)
    },
    //  文章详情
    PostDetail(id: number) {
        return service.get(`/posts?id=${id}`)
    },
    //  获取文章评论
    comments({ post, _limit, _start }: { post: number; _limit: number; _start: number }) {
        return service.get(`/posts/comments?post=${post}&_limit=${_limit}&_start=${_start}`)
    },
    //  推荐文章
    relevantPost(id: number) {
        return service.get(`/posts/recommend?id=${id}`)
    },
    //  推荐机票
    proposalAir() {
        return service.get(`/airs/sale`)
    },
    //  机票列表
    airList({ departCity, departCode, destCity, destCode, departDate }: { departCity: string; departCode: string; destCity: string; destCode: string; departDate: string }) {
        return service.get(`/airs?departCity=${departCity}&departCode=${departCode}&destCity=${destCity}&destCode=${destCode}&departDate=${departDate}`)
    },
    //  实时机票城市
    airCity(name: string) {
        return service.get(`/airs/city?name=${name}`)
    },
    //  选择机票
    getAirs(id: number) {
        return service.get(`/airs/${id}`)
    },
    //提交机票订单
    airorders({ users, insurances, contactName, contactPhone, invoice, seat_xid, air, captcha }: { users: object[]; insurances: string[]; contactName: string; contactPhone: string; invoice: boolean; seat_xid: string; air: number; captcha: string; }) {
        return service.post(`/airorders`, { users, insurances, contactName, contactPhone, invoice, seat_xid, air, captcha },
            {
                headers:
                    { Authorization: 'Bearer ' + token }
            })
    },
    //  查找省份下的城市
    searchCity(id: number) {
        return service.get(`/province/search?id=${id}`)
    },
    // 酒店选项
    gethotels() {
        return service.get(`/hotels/options`)
    },
    // 酒店详情
    hotelDetail({ id, city, price_in, hotellevel, hoteltype, hotelbrand, hotelasset, enterTime, leftTime, person, _limit, _start }: { id: number; city: number; price_in: number; hotellevel: number; hoteltype: number; hotelbrand: number; hotelasset: number; enterTime: Date; leftTime: Date; person: number; _limit: number, _start: number; }) {
        return service.get(`/hotels?id=${id}&city=${city}&price_in=${price_in}&hotellevel=${hotellevel}&hoteltype=${hoteltype}&hotelbrand=${hotelbrand}&hotelasset=${hotelasset}&enterTime=${enterTime}&leftTime=${leftTime}&person=${person}&_limit=${_limit}&_start=${_start}`)
    },
    //酒店列表
    hotelList({ city, enterTime, leftTime, _limit, _start }: { city: number; enterTime: string; leftTime: string; _limit: number; _start: number; }) {
        return service.get(`/hotels?city=${city}&enterTime=${enterTime}&leftTime=${leftTime}&_limit=${_limit}&_start=${_start}`)
    },
    //酒店列表2
    hotelLists(city: number) {
        return service.get(`/hotels?city=${city}`)
    },
    //酒店详情2
    getHotelD(id: number) {
        return service.get(`/hotels?id=${id}`)
    },
}
